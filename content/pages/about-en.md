Title: Who We Are
Slug: we-are-xoxzo
Lang: en
Date: 2012-12-01 10:02

# Xoxzo Inc.

Xoxzo Inc. was founded as MARIMORE Inc. in 2007 in Tokyo. We believe in the ROWE
working style, where we work remotely without offices.

To learn more about what we do, and who we are, please visit
[https://info.xoxzo.com/en/](https://info.xoxzo.com/en/)

