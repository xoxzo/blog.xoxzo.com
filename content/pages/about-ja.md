Title: 我々ゾクゾー
Slug: we-are-xoxzo
Lang: ja
Date: 2012-12-01 10:02

# Xoxzo Inc.

株式会社Xoxzoは、株式会社MARIMOREとして、2007年に東京にて設立された会社です。
わたしたちはROWEという働きかたを実践し、固定オフィスなしにノマド・ワークスタイルで日々仕事をこなしています。

私たちの今後の目標、提供するプロダクトなどは、[https://info.xoxzo.com/en/](https://info.xoxzo.com/en/)を
ご覧ください。

